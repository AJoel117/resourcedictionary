﻿//Joel Antony & Christopher Still
//Lab 6
//Style and resource dictionary in xaml page

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

//This is a simple calculator!
namespace CalculatorEx
{
    public partial class MainPage : ContentPage
    {

        double Val1;
        double Val2;
        double result;
        string operation;


        public MainPage()
        {
            InitializeComponent();
            displayNum.Text = "";
        }

        
        //These are the functions for the number buttons
        private void zeroButton_Clicked(object sender, EventArgs e)
        {
            displayNum.Text = displayNum.Text + "0";
        }

        private void sevenButton_Clicked(object sender, EventArgs e)
        {
            displayNum.Text = displayNum.Text + "7";
        }

        private void eightButton_Clicked(object sender, EventArgs e)
        {
            displayNum.Text = displayNum.Text + "8";
        }

        private void nineButton_Clicked(object sender, EventArgs e)
        {
            displayNum.Text = displayNum.Text + "9";
        }

        private void fourButton_Clicked(object sender, EventArgs e)
        {
            displayNum.Text = displayNum.Text + "4";
        }

        private void fiveButton_Clicked(object sender, EventArgs e)
        {
            displayNum.Text = displayNum.Text + "5";
        }

        private void sixButton_Clicked(object sender, EventArgs e)
        {
            displayNum.Text = displayNum.Text + "6";
        }

        private void oneButton_Clicked(object sender, EventArgs e)
        {
            displayNum.Text = displayNum.Text + "1";
        }

        private void twoButton_Clicked(object sender, EventArgs e)
        {
            displayNum.Text = displayNum.Text + "2";
        }

        private void threeButton_Clicked(object sender, EventArgs e)
        {
            displayNum.Text = displayNum.Text + "3";
        }

        //OPERATION BUTTONS

        private void clearButton_Clicked(object sender, EventArgs e)
        {
            if (displayNum.Text != null)
            {
                displayNum.Text = "";
            }
        }

        //This is for addition
        private void addButton_Clicked(object sender, EventArgs e)
        {
            if (displayNum.Text != null)
            {
                Val1 = double.Parse(displayNum.Text);
                displayNum.Text = "";
                operation = "add";
            }
        }

        //This is for subtraction
        private void subButton_Clicked(object sender, EventArgs e)
        {
            if (displayNum.Text != null)
            {
                Val1 = double.Parse(displayNum.Text);
                displayNum.Text = "";
                operation = "sub";
            }
        }

        //This is for Multiplication
        private void mulButton_Clicked(object sender, EventArgs e)
        {
            if (displayNum.Text != null)
            {
                Val1 = double.Parse(displayNum.Text);
                displayNum.Text = "";
                operation = "mul";
            }
        }

        //This is for division
        private void divButton_Clicked(object sender, EventArgs e)
        {
            if (displayNum.Text != null)
            {
                Val1 = double.Parse(displayNum.Text);
                displayNum.Text = "";
                operation = "div";
            }
        }

        //This calculated the value when the equal button is pressed
        private void equalButton_Clicked(object sender, EventArgs e)
        {
            if (displayNum.Text != null)
            {
                if (operation == "add")
                {
                    Val2 = double.Parse(displayNum.Text);
                    result = Val1 + Val2;
                    displayNum.Text = result.ToString();
                }
                else if (operation == "sub")
                {
                    Val2 = double.Parse(displayNum.Text);
                    result = Val1 - Val2;
                    displayNum.Text = result.ToString();
                }
                else if (operation == "mul")
                {
                    Val2 = double.Parse(displayNum.Text);
                    result = Val1 * Val2;
                    displayNum.Text = result.ToString();
                }
                else if (operation == "div")
                {
                    Val2 = double.Parse(displayNum.Text);
                    result = Val1 / Val2;
                    displayNum.Text = result.ToString();
                }

                Val1 = 0;
                Val2 = 0;
                result = 0;
            }
        }
    }
}